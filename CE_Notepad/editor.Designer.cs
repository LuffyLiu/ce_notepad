﻿namespace DeviceApplication2
{
	partial class editor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MainMenu mainMenu1;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( editor ) );
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuFile = new System.Windows.Forms.MenuItem();
			this.menuOpen = new System.Windows.Forms.MenuItem();
			this.menuSave = new System.Windows.Forms.MenuItem();
			this.menuExit = new System.Windows.Forms.MenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.fileContent = new System.Windows.Forms.TextBox();
			this.labelFilename = new System.Windows.Forms.Label();
			this.textBoxFilename = new System.Windows.Forms.TextBox();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.panel1 = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.Add( this.menuFile );
			// 
			// menuFile
			// 
			this.menuFile.MenuItems.Add( this.menuOpen );
			this.menuFile.MenuItems.Add( this.menuSave );
			this.menuFile.MenuItems.Add( this.menuExit );
			resources.ApplyResources( this.menuFile, "menuFile" );
			// 
			// menuOpen
			// 
			resources.ApplyResources( this.menuOpen, "menuOpen" );
			this.menuOpen.Click += new System.EventHandler( this.menuOpen_Click );
			// 
			// menuSave
			// 
			resources.ApplyResources( this.menuSave, "menuSave" );
			this.menuSave.Click += new System.EventHandler( this.menuSave_Click );
			// 
			// menuExit
			// 
			resources.ApplyResources( this.menuExit, "menuExit" );
			this.menuExit.Click += new System.EventHandler( this.menuExit_Click );
			// 
			// openFileDialog
			// 
			this.openFileDialog.InitialDirectory = "\\";
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.InitialDirectory = "\\";
			// 
			// fileContent
			// 
			this.fileContent.AcceptsTab = true;
			resources.ApplyResources( this.fileContent, "fileContent" );
			this.fileContent.Name = "fileContent";
			this.fileContent.KeyDown += new System.Windows.Forms.KeyEventHandler( this.fileContent_KeyDown );
			this.fileContent.KeyUp += new System.Windows.Forms.KeyEventHandler( this.fileContent_KeyUp );
			// 
			// labelFilename
			// 
			resources.ApplyResources( this.labelFilename, "labelFilename" );
			this.labelFilename.Name = "labelFilename";
			// 
			// textBoxFilename
			// 
			this.textBoxFilename.BackColor = System.Drawing.SystemColors.InactiveCaption;
			resources.ApplyResources( this.textBoxFilename, "textBoxFilename" );
			this.textBoxFilename.Name = "textBoxFilename";
			this.textBoxFilename.ReadOnly = true;
			this.textBoxFilename.TabStop = false;
			// 
			// statusBar1
			// 
			resources.ApplyResources( this.statusBar1, "statusBar1" );
			this.statusBar1.Name = "statusBar1";
			// 
			// panel1
			// 
			resources.ApplyResources( this.panel1, "panel1" );
			this.panel1.Name = "panel1";
			// 
			// editor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 96F, 96F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			resources.ApplyResources( this, "$this" );
			this.Controls.Add( this.statusBar1 );
			this.Controls.Add( this.textBoxFilename );
			this.Controls.Add( this.labelFilename );
			this.Controls.Add( this.fileContent );
			this.Controls.Add( this.panel1 );
			this.Menu = this.mainMenu1;
			this.Name = "editor";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.TextBox fileContent;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuOpen;
		private System.Windows.Forms.MenuItem menuSave;
		private System.Windows.Forms.MenuItem menuExit;
		private System.Windows.Forms.Label labelFilename;
		private System.Windows.Forms.TextBox textBoxFilename;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.Panel panel1;


	}
}