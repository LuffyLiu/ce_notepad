﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using Microsoft.Win32;

namespace DeviceApplication2
{
	public partial class editor : Form
	{
		public editor()
		{
			InitializeComponent();
			UpdateRegistry();
			CopyExecuteFile();
		}

		public editor( string szfilePath )
		{
			InitializeComponent();
			ReadFile( szfilePath );
			textBoxFilename.Text = szfilePath;
			saveFileDialog.FileName = szfilePath;
		}

		private bool bCtrlhold = false;

		private void ReadFile( string szfilePath )
		{
			System.IO.StreamReader soureceFile = new System.IO.StreamReader( szfilePath );
			fileContent.Text = soureceFile.ReadToEnd();
			soureceFile.Close();
		}

		private void WriteFile( string szFilePath )
		{
			System.IO.StreamWriter resultFile = new System.IO.StreamWriter( szFilePath );
			resultFile.Write( fileContent.Text );
			resultFile.Close();
		}

		private void SaveFile()
		{
			//還在就存起來
			if( System.IO.File.Exists( saveFileDialog.FileName ) ) {
				WriteFile( saveFileDialog.FileName );
			}
			//不在的話另存新檔
			else if( saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK ) {
				WriteFile( saveFileDialog.FileName );
			}
		}

		//open the file when we click the open file
		private void menuOpen_Click( object sender, EventArgs e )
		{
			if( openFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK ) {
				return;
			}
			ReadFile( openFileDialog.FileName );
			textBoxFilename.Text = openFileDialog.FileName;
			saveFileDialog.FileName = openFileDialog.FileName;
		}

		//save the file when we click the save file
		private void menuSave_Click( object sender, EventArgs e )
		{
			SaveFile();
		}

		private void menuExit_Click( object sender, EventArgs e )
		{
			this.Close();
		}

		// Update windows registry for extension file name(.lad) and file icon
		private void UpdateRegistry()
		{
			// Update registry key for extension file name ".xml"
			UpdateStringKey( Registry.ClassesRoot, @".xml", @"", @"txt file" );

			// Update registry key for extension file name ".txt"
			UpdateStringKey( Registry.ClassesRoot, @".txt", @"", @"txt file" );

			// Update registry key for execute file path in HKEYS_CLASSES_ROOT
			UpdateStringKey( Registry.ClassesRoot, @"txt file\shell\open\command", @"", @"CE_Notepad.exe ""%1""" );

			// Update registry key for default icon with ".lad" files
			UpdateStringKey( Registry.ClassesRoot, @"txt file\DefaultIcon", @"", @"CE_Notepad.exe,0" );
		}

		private void CopyExecuteFile()
		{
			string szSrcPath = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
			string szDesPath = @"\DiskC\Shared\Windows\CE_Notepad.exe";

			if( File.Equals( szSrcPath, szDesPath ) ) {
				return;
			}

			File.Copy( szSrcPath, szDesPath, true );
		}

		// Update windows registry key with assigned value
		private void UpdateStringKey( RegistryKey RegRoot, string szSubKey, string szKeyName, string szNewKeyValue )
		{
			// 1. Check registry key exists or not. If not exist, create the key.
			RegistryKey Key = RegRoot.OpenSubKey( szSubKey, true );

			if( Key == null ) {
				Key = RegRoot.CreateSubKey( szSubKey );
			}

			// 2. Update the key value.
			Key.SetValue( szKeyName, szNewKeyValue, RegistryValueKind.String );

			Key.Close();
		}

		private void fileContent_KeyDown( object sender, KeyEventArgs e )
		{
			switch( e.KeyCode ) {
				case Keys.ControlKey:
					bCtrlhold = true;
					break;

				case Keys.S:
					if( bCtrlhold == false ) {
						return;
					}
					SaveFile();
					break;

				case Keys.Q:
					if( bCtrlhold == false ) {
						return;
					}
					this.Close();
					break;
			}
		}

		private void fileContent_KeyUp( object sender, KeyEventArgs e )
		{
			switch( e.KeyCode ) {
				case Keys.ControlKey:
					bCtrlhold = false;
					break;
			}
		}
	}
}