﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DeviceApplication2
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[MTAThread]
		static void Main( string[] argv )
		{
			if( argv.Length > 0 ) {
				Application.Run( new editor( argv[ 0 ] ) );
				return;
			}

			Application.Run( new editor() );
		}
	}
}